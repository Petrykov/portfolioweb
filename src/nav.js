import 'bootstrap/dist/css/bootstrap.min.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faBars} from '@fortawesome/free-solid-svg-icons'
import React, {useState} from 'react';
import {BrowserRouter as Router, Link, Route,} from "react-router-dom";
import Home from "./Home/Home";
import AboutMe from "./AboutMe/AboutMe";
import Contacts from "./Contacts/Contacts";


class NavC extends React.Component {

    constructor(props) {
        super(props);
        this.state = {collapsed: true, setCollapsed: true};
    }

    componentDidMount() {
        this.refresh();
    }

    // responsive(e) {
    //     let x = document.getElementById("myTopnav");
    //     if (x.className === "topnav") {
    //         x.className += " responsive";
    //     } else {
    //         x.className = "topnav";
    //     }
    // }

    highlight(e, id) {// search avtive, make it normal
        document.getElementsByClassName("link_active")[0].setAttribute("class", "link");
        // make it active
        document.getElementById(id).className = "link_active";
    }

    refresh() {
        let url = window.location.href;
        let split = url.split('/');
        let current = split[3];
        console.log('C: ' + current);

        let id = -1;

        switch (current) {
            case '':
                id = 1;
                break;

            case 'aboutMe':
                id = 2;
                break;

            case 'contacts':
                id = 3;
                break;
        }

        let li = document.getElementsByTagName('a');


        for (let i = 0; i < 3; i++) {
            console.log('li lenght: ' + li.length);
            console.log('id found ' + id);
            if (li[i].getAttribute('id') === "" + id) {
                console.log('key is: ' + li[i].getAttribute('id'));
                li[i].setAttribute('class', 'link_active');
            } else {
                li[i].setAttribute('class', 'link');
            }
        }
    }

    responsive() {
        let x = document.getElementById("myNav");
        let padding = document.getElementsByClassName("icon-div")[0];
        if (x.className === "topnav") {
            x.className += " responsive";
            padding.setAttribute("height", "0px");
        } else {
            x.className = "topnav";
            padding.setAttribute("height", "60px");
        }


    }

    render() {

        return <div>

            <div className="topnav" id="myNav">

                <div>
                    <a href="/" id="1" onClick={e => this.highlight(e, '1')}>Home</a>

                    <a href="/aboutMe" id="2" onClick={e => this.highlight(e, '2')}>About Me</a>

                    <a href="/contacts" id="3" onClick={e => this.highlight(e, '3')}>Contacts</a>

                </div>

                <div className="icon-div">

                    <a href="javascript:void(0);" className="icon" onClick={this.responsive}>
                        <FontAwesomeIcon icon={faBars} color="black"/>
                    </a>

                </div>
            </div>
            <Route exact path="/" component={Home}/>
            <Route path="/aboutMe" component={AboutMe}/>
            <Route path="/contacts" component={Contacts}/>
        </div>
    }
}

export default NavC;