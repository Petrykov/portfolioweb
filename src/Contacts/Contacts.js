import {Button, Col, Form, FormGroup, Label, Input} from 'reactstrap';
import * as emailjs from "emailjs-com";
import Footer from '/Users/vpetrykov/Desktop/IdeaProjects/mywebsite/src/f00ter.js'
import React, {useCallback, Fragment} from 'react'
import ReactDOM from 'react-dom'
import {useSpring, animated, interpolate} from 'react-spring'
import { useAlert } from 'react-alert'
import { Toast, ToastBody, ToastHeader } from 'reactstrap';


class Contacts extends React.Component {

    state = {
        name: '',
        email: '',
        subject: '',
        message: '',
        fieldsField: false
    };

    onButtonClick = (e) => {
        // const[alert] = useAlert();
        // alert.show("Message was sent successfully");
    };

    handleSubmit(e) {
        console.log('clicked');
        if (this.state.fieldsField === false) {
            let email = document.getElementsByClassName('form-control');

            for (let i = 0; i <email.length ; i++) {
                if(email[i].value === ''){
                    console.log('not all field are filled');
                    return;
                }
            }

            this.setState({
                fieldsField: true
            });

        } else {
            console.log('filled');
            this.setState({
                fieldsField: false
            });
        }

        // e.preventDefault();
        // const {name, email, subject, message} = this.state;
        // let templateParams = {
        //     from_name: email,
        //     to_name: 'vpetrikov15@gmail.com',
        //     subject: subject,
        //     message_html: message,
        // };
        // emailjs.send(
        //     'mailjet',
        //     'template_TLLJv1Vu',
        //     templateParams,
        //     'user_FhzhrdpVGH6C3rSNuNe2g'
        // ).then(function (response) {
        //     console.log('SUCCESS! ');
        // }, (function (err) {
        //     console.log("ERROR ");
        // }));
        // this.resetForm();
    }

    resetForm() {
        this.setState({
            name: '',
            email: '',
            subject: '',
            message: '',
        })
    }

    handleChange = (param, e) => {
        this.setState({[param]: e.target.value})
    };

    constructor(props) {
        super(props);
    };

    componentDidMount() {

    };

    render() {
        return <div className="body-home">

            {/*<div className="effectcs_3d">*/}
            {/*    <App/>*/}
            {/*</div>*/}
            <h1>You can contact me by mail:</h1>
            <div className="div-contact">
                <Form>
                    <FormGroup row>
                        <Label for="exampleEmail" sm={2}>Email</Label>
                        <Col sm={10}>
                            <Input value={this.state.email} onChange={this.handleChange.bind(this, 'email')}
                                   type="email" name="email" id="exampleEmail" placeholder="Enter your email"/>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="exampleName" sm={2}>Name</Label>
                        <Col sm={10}>
                            <Input value={this.state.name} onChange={this.handleChange.bind(this, 'name')} type="name"
                                   name="name"
                                   id="exampleName" placeholder="Enter your name"/>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="exampleSubject" sm={2}>Subject</Label>
                        <Col sm={10}>
                            <Input value={this.state.subject} onChange={this.handleChange.bind(this, 'subject')}
                                   type="subject" name="subject"
                                   id="exampleSubject" placeholder="Enter your subject"/>
                        </Col>
                    </FormGroup>

                    <FormGroup row>
                        <Label for="exampleText" sm={2}>Text Area</Label>
                        <Col sm={10}>
                            <Input value={this.state.message} onChange={this.handleChange.bind(this, 'message')}
                                   placeholder="Your text goes here" type="textarea" name="text" id="exampleText"/>
                        </Col>
                    </FormGroup>
                </Form>

                <Fragment>
                    <div className="btn_submit">
                        <Button variant="primary" type="submit" onClick={this.handleSubmit.bind(this)}>
                            Submit
                        </Button>
                    </div>
                </Fragment>
            </div>
           <Footer/>
        </div>
    }
}

export default Contacts;
