import styled, {keyframes} from 'styled-components';
import {bounceInDown, fadeIn} from 'react-animations';
import * as THREE from 'three'
import React, {Suspense, useCallback, useRef, useMemo} from 'react'
import {Canvas, useFrame} from 'react-three-fiber'
// import Effects from './Effects'
import Footer from '/Users/vpetrykov/Desktop/IdeaProjects/mywebsite/src/f00ter.js'

document.body.style.fontFamily = 'Tomorrow';

const FadeIn = styled.div`animation: 2s ${keyframes`${fadeIn}`}`;
const Bounce = styled.div`animation: 2s ${keyframes`${bounceInDown}`}`;


function Swarm({count, mouse}) {
    const mesh = useRef();
    const dummy = useMemo(() => new THREE.Object3D(), []);

    const particles = useMemo(() => {
        const temp = [];
        for (let i = 0; i < count; i++) {
            const t = Math.random() * 100;
            const factor = 20 + Math.random() * 100;
            const speed = 0.01 + Math.random() / 200;
            const xFactor = -20 + Math.random() * 65;
            const yFactor = -20 + Math.random() * 65;
            const zFactor = -20 + Math.random() * 65;
            temp.push({t, factor, speed, xFactor, yFactor, zFactor, mx: 0, my: 0})
        }
        return temp
    }, [count]);

    useFrame(state => {
        particles.forEach((particle, i) => {
            let {t, factor, speed, xFactor, yFactor, zFactor} = particle;
            //set speed of the circles
            t = particle.t += speed / 2;
            const a = Math.cos(t) + Math.sin(t * 1) / 10;
            const b = Math.sin(t) + Math.cos(t * 2) / 10;
            // size
            const s = Math.max(1.5, Math.cos(t) * 3);
            particle.mx += (mouse.current[0] - particle.mx) * 0.02;
            particle.my += (-mouse.current[1] - particle.my) * 0.02;
            dummy.position.set(
                (particle.mx / 20) * a + xFactor + Math.cos((t / 10) * factor) + (Math.sin(t * 1) * factor) / 5,
                (particle.my / 20) * b + yFactor + Math.sin((t / 10) * factor) + (Math.cos(t * 2) * factor) / 5,
                (particle.my / 20) * b + zFactor + Math.cos((t / 10) * factor) + (Math.sin(t * 3) * factor) / 5
            );
            dummy.scale.set(s, s, s);
            dummy.updateMatrix();
            mesh.current.setMatrixAt(i, dummy.matrix);
            //rotate
            // mesh.current.rotation.y += 0.00001;
            // mesh.current.rotation.x += 0.00001;
        });
        mesh.current.instanceMatrix.needsUpdate = true
    });

    return (
        <>
            <instancedMesh ref={mesh} args={[null, null, count]}>
                <sphereBufferGeometry attach="geometry" args={[1, 32, 32]}/>
                <meshPhongMaterial attach="material" color="white"/>
            </instancedMesh>
        </>
    )
}


function App() {
    const mouse = useRef([0, 0]);
    const onMouseMove = useCallback(({clientX: x, clientY: y}) => (mouse.current = [x - window.innerWidth / 2, y - window.innerHeight / 2]), []);
    return (

        <div className="circles_3d" onMouseMove={onMouseMove}>
            <Canvas
                gl={{alpha: false, antialias: false, logarithmicDepthBuffer: true}}
                camera={{fov: 75, position: [0, 0, 70]}}
                onCreated={({gl}) => {
                    gl.setClearColor('#e2dfd0');
                    gl.toneMapping = THREE.ACESFilmicToneMapping;
                    gl.outputEncoding = THREE.sRGBEncoding
                }}>
                <ambientLight intensity={0.1}/>
                <pointLight position={[100, 100, 100]} intensity={5} color="#e2dfd0"/>
                <pointLight position={[-100, -100, -100]} intensity={5} color="#cec9b1"/>
                <Swarm mouse={mouse} count={150}/>
                <Suspense fallback={null}>
                    {/*<Effects />*/}
                </Suspense>
            </Canvas>
        </div>
    )
}

class Home extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <>

            <div className="container_home">

                <App/>

                <div className="home_div">

                    <div className="header">
                        <FadeIn><h1>Brief introduction</h1></FadeIn>
                    </div>

                    <div className="underline"/>

                    <div className="info">

                        <div className="saxion_photo">
                            <Bounce><img src="logo.png"/></Bounce>
                        </div>

                        <div className="about_me">
                            <FadeIn>
                                <h2>Hello. My name is Vladyslav.
                                    <br/>I'm a second year student at the Saxion studying a Software Engineering program and
                                    right now i'm looking for a place for the internship.</h2>
                            </FadeIn>
                        </div>
                    </div>
                </div>

            </div>
        <Footer/>
        </>
    }
}


export default Home;