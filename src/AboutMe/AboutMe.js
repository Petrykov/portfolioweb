import React from 'react';
import styled, {keyframes} from 'styled-components';
import {bounceInDown} from 'react-animations';
import {bounceInUp} from 'react-animations';
import {rotateInDownRight} from 'react-animations';
import {rollIn} from 'react-animations';
import Slider from 'infinite-react-carousel';
import Footer from '/Users/vpetrykov/Desktop/IdeaProjects/mywebsite/src/f00ter.js'

import {
    Card, Button, CardImg, CardTitle, CardText, CardColumns,
    CardSubtitle, CardBody
} from 'reactstrap';

const BounceFromTop = styled.div`animation: 2s ${keyframes`${bounceInDown}`}`;
const BounceFromBottom = styled.div`animation: 2s ${keyframes`${bounceInUp}`}`;
const Rotate = styled.div`animation: 2s ${keyframes`${rotateInDownRight}`}`;
const Rollin = styled.div`animation: 2s ${keyframes`${rollIn}`}`;


const VapeshopSlider = () => (
    <Slider dots>
        <div className="carousel_image">
            <img src="vapeshop1.png"/>
        </div>

        <div className="carousel_image">
            <img src="vapeshop5.png"/>
        </div>

        <div className="carousel_image">
            <img src="vapeshop3.png"/>
        </div>

        <div className="carousel_image">
            <img src="vapeshop4.png"/>
        </div>

        <div className="carousel_image">
            <img src="vapeshop2.png"/>
        </div>

        <div className="carousel_image">
            <img src="vapeshop6.png"/>
        </div>

        <div className="carousel_image">
            <img src="vapeshop7.png"/>
        </div>
    </Slider>
);

const CoachPlatsSlider = () => (
    <Slider dots>

        <div className="carousel_image">
            <img src="cp9.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp8.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp7.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp6.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp5.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp3.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp2.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp4.png"/>
        </div>

        <div className="carousel_image">
            <img src="cp1.png"/>
        </div>

    </Slider>
);



const FBI = () => (
    <Slider dots>
        <div className="carousel_image">
            <img src="fbi2.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi1.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi13.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi12.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi10.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi11.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi7.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi8.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi9.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi6.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi5.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi4.png"/>
        </div>

        <div className="carousel_image">
            <img src="fbi3.png"/>
        </div>

    </Slider>
);

class StudentPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            opened: false
        };
        this.toggleBox = this.toggleBox.bind(this);
    }

    toggleBox() {
        const {opened} = this.state;
        if (this.state.opened === false) {
            document.getElementsByClassName("about_me_div")[0].setAttribute("style", "opacity: 20%");
            document.getElementsByClassName("projects")[0].setAttribute("style", "opacity: 20%");
        } else {
            document.getElementsByClassName("about_me_div")[0].setAttribute("style", "opacity: 100%");
            document.getElementsByClassName("projects")[0].setAttribute("style", "opacity: 100%");
        }

        this.setState({
            opened: !opened,
        });
    }

    componentDidMount() {

    }

    render() {
        let {title, children} = this.props;
        const {opened} = this.state;

        if (opened) {
            title = 'Hide Vehicles';
            console.log('hidden');
        } else {
            title = 'Show Vehicles';
            console.log('show');
        }

        return <>
            <div>
                <div className="body">
                    <BounceFromTop>
                        <div className="about_me_div" onClick={this.toggleBox}>
                            <div className="skills_header">

                                <div className="my_image">
                                    <img src="myPhoto_circled.jpg"/>
                                </div>

                                <div className="skills">
                                    <h1>Skills: </h1>
                                    <ul>
                                        <li>JAVA(Android)</li>
                                        <li>HTML CSS</li>
                                        <li>Javascript</li>
                                        <li>C++ (OOP lvl)</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        {opened && (
                            <Rollin>
                                <div className="explicitInfo">
                                    <h1>To start with as I already mentioned I'm familiar with Java(application development 'Android'), C++(OOP level), JS(nodeJS,flutter), HTML/CSS.
                                        You can have a look at the real application that was partly/solo developed by me.
                                        Each of the team projects was done using Agile technology.
                                        Most of the time I've been learning JAVA , during this time I've learned threading, socket connections apart from the OOP level.
                                        I'm also very excited about working with rest API.
                                        I have also skills in working with remote databases (Firebase) and GIT(lab/hub).  </h1>
                                </div>
                            </Rollin>

                        )}

                    </BounceFromTop>

                    <BounceFromBottom>
                        <div className="projects">

                            <div className="projects_header">
                                <h1>Projects</h1>
                            </div>

                            <div className="cards_list">
                                <CardColumns>

                                    <Card>
                                        <CardImg className="server-client-img" top width="100%" src="server.png" alt="Card image cap"/>
                                        <CardBody>
                                            <CardTitle>Server</CardTitle>
                                            <CardText>- In this project, I implemented the entire server which listens to all new clients who want to connect to it.
                                                <br/><br/>- There's a multiple function user could choose on the server: request a list of online users, write a direct message to another user, create group, join group, pass the file to another user... etc. Before being logged in to the server there is a check if a user with the name new user wants to log in already exists and whether the name matches required criteria.
                                                <br/><br/>- The server also holds the public keys of all connected users so that the direct communication between two clients is fully encrypted (using symmetric encryption).
                                            </CardText>
                                            <a href="https://gitlab.com/Petrykov/server_internet_technology"> GitLab repository </a>
                                        </CardBody>
                                    </Card>

                                    <Card>
                                        <VapeshopSlider/>
                                        <CardBody>
                                            <CardTitle>Vape 4 You</CardTitle>
                                            <CardText>- This is my first solo Android Project that I've built for 8 weeks.
                                                <br/><br/>- During the Android Programming course, i taught how data could be passed from one layout to another.
                                                I learned some very basic principles of design and of combining designed elements with the logic.
                                                <br/><br/>- The idea behind this application is that you have a possibility as an administrator to add/remove/delete objects from different lists.
                                            </CardText>
                                            <a href="https://gitlab.com/Petrykov/vapeshop"> GitLab repository </a>
                                        </CardBody>
                                    </Card>

                                    <Card>
                                        <CoachPlatsSlider/>
                                        <CardBody>
                                            <CardTitle>Coachplaats</CardTitle>
                                            <CardText>- This project was developed in a team of 4 developers. One person was mostly focused on the design part, another at allocating a local DB for this project and I with one more guy were working on the implementation of the functionality.
                                                <br/><br/>- The task was to build an app in which both clients/customers could find each other. For this, there was introduced a possibility of creating an account. After the customer/client log in to the app there's a banch of different possibilities to do. Both the client and coach could change their personal information and photos.
                                                <br/><br/>- The coach by default has 'unverified' status => client couldn't find him/her. Only after the administrator would have a look at the document uploaded by the coach he/she can be verified.
                                                <br/><br/>- Check the documentation attached in .pdf file to have a more clear understanding of the app.
                                            </CardText>
                                            <a href="https://gitlab.com/saxion.nl/project-persistent/coach-plaats/02"> GitLab repository </a>
                                        </CardBody>
                                    </Card>

                                    <Card>
                                        <CardImg top width="100%" className="server-client-img" src="client.png" alt="Card image cap"/>
                                        <CardBody>
                                            <CardTitle>Client</CardTitle>
                                            <CardText>- The idea of this project is that the client could communicate on established protocols that were defined from the server-side.
                                                <br/><br/>- Basically, the client simply sends all the requests to the server and listens to the responses.
                                            </CardText>
                                            <a href="https://gitlab.com/Petrykov/internet_technology"> GitLab repository </a>
                                        </CardBody>
                                    </Card>

                                    <Card>
                                        <FBI/>
                                        <CardBody>
                                            <CardTitle>FBS</CardTitle>
                                            <CardText>- This project was developed in a team of 20 people. 2 groups of 5 persons were working on app design/functionality, rest were setting up a server and writing an API.
                                                <br/><br/>- I developed a sidebar menu and made several connections to the rest API requests to the app. I was also a SCRUM master of our sub-team and a person who is responsible for merging closed issues. I think we did a pretty good job for the first time working in such a big team. Still, there are some parts that are not working properly according to the discussed fields in DB for example. As a result, the price of the event is not functioning properly.
                                            </CardText>
                                            <a href="https://gitlab.com/saxion.nl/server-clients/fbs"> GitLab repository </a>
                                        </CardBody>
                                    </Card>

                                </CardColumns>
                            </div>
                        </div>
                        {/*<Footer/>*/}
                    </BounceFromBottom>
                </div>
            </div>
        </>
    }
}

export default StudentPage;