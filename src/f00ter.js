import 'bootstrap/dist/css/bootstrap.min.css';
import React, {useState} from 'react';
import {animated, useSpring} from "react-spring";


const calc = (x, y) => [-(y - window.innerHeight / 2) / 20, (x - window.innerWidth / 2) / 20, 1.1];
const trans = (x, y, s) => `perspective(600px) rotateX(${x}deg) rotateY(${y}deg) scale(${s})`;

function Animate() {
    const [props, set] = useSpring(() => ({ xys: [0, 0, 1], config: { mass: 5, tension: 350, friction: 40 } }));
    return (
        <animated.div
            className="footer"
            onMouseMove={({ clientX: x, clientY: y }) => set({ xys: calc(x, y) })}
            onMouseLeave={() => set({ xys: [0, 0, 1] })}
            style={{ transform: props.xys.interpolate(trans) }}
        > <h1>Designed and developed by Vladyslav Petrykov
            <br/>2020 | 461157@student.saxion.nl</h1>
        </animated.div>
    )
}

class NavC extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    render() {
        return <div className="footer">
            <Animate/>
        </div>
    }
}

export default NavC;